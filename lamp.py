# -*- coding: utf-8 -*-
# ! python3
# !/usr/bin/env python3
from datetime import datetime
from tinydb import TinyDB, Query
import moment
import signal
import sys

# lamp.insert({"name": "231", "notes": "We need a more powerfull one",
#               "installation date": "2016-07-02", "Urgent notes": "",
#                "type": "GE T19 1000W, GX9.5, 109.18kr", "working": True})
# lamp.insert({"name": "42", "notes": "Test notes",
#              "installation date": "2016-08-21",
#              "Urgent notes": "Need To add more data to this DB",
#              "type": "OSRAM PAR 64 1000W MFL CP62 AluPAR, 157.60kr",
#              "working": False})
# types.insert({"name": "GE T19 1000W, GX9.5, 109.18kr"})
# types.insert({"name": "OSRAM PAR 64 1000W MFL CP62 AluPAR, 157.60kr"})

DEBUGG = False

if "-v" in sys.argv:
    DEBUGG = True

if "-help" in sys.argv:
    help()

if DEBUGG:
    print(lampDB.all())
    print(typesDB.all())
    print(sys.version)


def updateDB():

    # set the DB
    global db
    db = TinyDB('lampDB.json')

    # split lamps and type to diffrent Tables
    global lampDB
    global typesDB
    lampDB = db.table('lamp')
    typesDB = db.table('types')

    global lampALL
    global typesALL
    lampALL = lampDB.all()
    typesALL = typesDB.all()


def yes_no(question, default="yes"):
    # from http://stackoverflow.com/a/3041990 mod for python3
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def signal_handler(signal, frame):
    # from http://stackoverflow.com/a/4205386
    print('\nQuitting')
    sys.exit(0)


def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return abs((d2 - d1).days)


def add():
    "adds lamp to db"
    name = input("What is the name/position number? ")
    print("\nWhat is the lamp type? ")

    # set counter to pick. loop through to add counter and print
    counter = 1
    print("0) Set new lamp type")
    for types in typesALL:
        print(str(counter) + ") " + types['name'])
        counter += 1

    # pick number and print the picked item
    iTypes = input("\nPick a number: ")
    if iTypes == "":
        print("invalid input, Aborted")
        return
    iTypes = int(iTypes)
    if iTypes == 0:
        newLampType = str(input("What is the new type? "))
        if yes_no("Are you sure?"):

            # SAVE TO DB
            # Adds the new name to the DB
            typesDB.insert({"name": str(newLampType)})

            lampType = newLampType
        else:
            return
    else:
        iTypes -= 1
        lampType = str(typesALL[iTypes]['name'])

    print("You picked: " + lampType)

    print("Enter = Skip")
    uNote = input("Urgent notes: ")
    note = input("Other notes: ")

    print("(if null = today)," +
          str(moment.now().format("YYYY-MM-DD")) + " (YYYY-MM-DD)")
    iDate = input("Installation Date: ")
    if iDate == "":
        iDate = str(moment.now().format("YYYY-MM-DD"))
    print("Skip = true, False -> false, f")
    working = input("Working: ")
    if working == "":
        working = True

    print("")
    print("Name: " + name)
    print("Type: " + lampType)
    print("Installation date: " + iDate)
    print("Notes: " + note)
    print("Urgent notes: " + uNote)
    print("Working: " + str(working))
    print("")

    if yes_no("Is the data correct?"):
        lampDB.insert({"name": str(name),
                       "notes": note,
                       "installation date": iDate,
                       "Urgent notes": uNote,
                       "type": lampType,
                       "working": working})

    else:
        print("Aborted")


def list_():
    "list all lamps and calc the time"
    print("\nList all lamps:\n")

    lampList = {}
    noResults = True
    for lamps in lampALL:
        noResults = False
        print("Name: " + lamps['name'])
        print("Type: " + lamps['type'])
        print("Installation date: " + lamps['installation date'])
        print("Notes: " + lamps['notes'])
        print("Urgent notes: " + lamps['Urgent notes'])
        print("Working: " + str(lamps['working']))
        print("")

        iDate = str(lamps['installation date'])
        days = days_between(iDate, moment.now().format("YYYY-MM-DD"))
        lampList.update({str(lamps['name']): days})

    if noResults:
        print("It looks like there is no data in the DB,"
              " Please add it with the add command")

    # form http://stackoverflow.com/a/16772088
    for lampName, key in sorted(lampList.items(), key=lambda x: x[1]):
        print("Name: %s: %s days since installation" % (lampName, key))
    # print(lampList)


def broken():
    "mark lamp as broken"
    # What lamp
    name = input("What is the name/position number? ")
    lamp = Query()
    noResults = True
    for lamps in lampDB.search(lamp['name'] == name):
        print("Got results")
        noResults = False

    if noResults:
        print("Did not found the lamp")
        if yes_no("Do you what to list all lamps"):
            print("")
            counter = 0

            for lamps in lampALL:
                print(str(counter) + ") name: " + lamps['name'])
                counter += 1

            ilampName = input("\nPick a number: ")
            if ilampName == "":
                print("invalid input, Aborted")
                return

            ilampName = int(ilampName)
            print("DEBUGG ilampName: ", ilampName)
            name = str(lampALL[ilampName]['name'])
            print("DEBUGG name: ", name)
        else:
            return

    lamps[0] = lampDB.search(lamp['name'] == name)
    print("")
    print("Name: " + lamps['name'])
    print("Type: " + lamps['type'])
    print("Installation date: " + lamps['installation date'])
    print("Notes: " + lamps['notes'])
    print("Urgent notes: " + lamps['Urgent notes'])
    print("Working: " + str(lamps['working']))

    if yes_no("\nIs this lamp broken?"):
        lampDB.update({'working': False}, lamp['name'] == name)
        print("DB updated")

    # get the name, check if is the db if not pick with number picker
    # are you sure
    # save to DB


def update():
    "update the data in the db"

    name = input("What is the name/position number? ")
    lamp = Query()
    noResults = True
    for lamps in lampDB.search(lamp['name'] == name):
        # print("Got results")
        noResults = False
        # print("lamp: " + str(lamps))
        print("")
        print("0) Name: " + lamps['name'])
        print("1) Type: " + lamps['type'])
        print("2) Installation date: " + lamps['installation date'])
        print("3) Notes: " + lamps['notes'])
        print("4) Urgent notes: " + lamps['Urgent notes'])
        print("5) Working: " + str(lamps['working']))

        iTypes = input("\nPick a number: ")
        if iTypes == "":
            print("invalid input, Aborted")
            return
        iTypes = int(iTypes)

        if iTypes == 0:
            newData = input("What is the new name? ")
            if yes_no("\nAre you sure?"):
                lampDB.update({'name': newData}, lamp['name'] == name)
                print("DB updated")
            else:
                return

        elif iTypes == 1:
            newData = input("What is the new type? ")
            if yes_no("\nAre you sure?"):
                lampDB.update({'type': newData}, lamp['name'] == name)
                print("DB updated")
            else:
                return

        elif iTypes == 2:
            newData = input("What is the new installation date? ")
            if yes_no("\nAre you sure?"):

                lampDB.update({
                    'installation date': newData},
                    lamp['name'] == name)

                print("DB updated")
            else:
                return

        elif iTypes == 3:
            newData = input("What is the new note? ")
            if yes_no("\nAre you sure?"):
                lampDB.update({'notes': newData}, lamp['name'] == name)
                print("DB updated")
            else:
                return

        elif iTypes == 4:
            newData = input("What is the new urgent note? ")
            if yes_no("\nAre you sure?"):
                lampDB.update({'Urgent notes': newData}, lamp['name'] == name)
                print("DB updated")
            else:
                return

        elif iTypes == 5:
            newData = input("What is the new working status? ")
            if yes_no("\nAre you sure?"):
                lampDB.update({'working': newData}, lamp['name'] == name)
                print("DB updated")
            else:
                return

    if noResults:
        print("Did not found the lamp")
        return

    #    for key, value in lamps.items():
    #        print(str(key)+":"+str(value))
    # what lamp to edit with number picker
    # what data? number picker
    # are you sure
    # update it to db


def status():
    "status"

    lamp = Query()
    fprintW = True
    for lamps in lampDB.search(lamp['working'] == False):
        if fprintW:
            print("\nThis lamps are marked as not working:")
            fprintW = False
        print("")
        print("Name: " + lamps['name'])
        print("Type: " + lamps['type'])
        print("Installation date: " + lamps['installation date'])
        print("Notes: " + lamps['notes'])
        print("Urgent notes: " + lamps['Urgent notes'])
        print("Working: " + str(lamps['working']))

    lamp = Query()
    fprintU = True
    for lamps in lampDB.search(lamp['Urgent notes'] != ""):
        if fprintU:
            print("\nThis lamps are marked as having something urgent:")
            fprintU = False
        print("")
        print("Name: " + lamps['name'])
        print("Type: " + lamps['type'])
        print("Installation date: " + lamps['installation date'])
        print("Notes: " + lamps['notes'])
        print("Urgent notes: " + lamps['Urgent notes'])
        print("Working: " + str(lamps['working']))

    if fprintU and fprintW:
            print("No urgent notes and all lamps seems to be working")

def remove():
    "remove"
    # what lamp to edit with number picker
    # are you sure (x2?)
    # update db


def fix():
    "mark lamp as working again"
    # get all lamp that are not working
    lamp = Query()
    fprintW = True
    for lamps in lampDB.search(lamp['working'] == False):
        if fprintW:
            print("\nThis lamps are marked as not working:")
            fprintW = False
        print("")
        print("Name: " + lamps['name'])
        print("Type: " + lamps['type'])
        print("Installation date: " + lamps['installation date'])
        print("Notes: " + lamps['notes'])
        print("Urgent notes: " + lamps['Urgent notes'])
        print("Working: " + str(lamps['working']))

    lamp = Query()
    counter = 0
    print("")
    wlist = []
    oneP = ""
    for lamps in lampDB.search(lamp['working'] == False):
        wlist.append(lamps['name'])
        oneP = oneP + str(counter) + ") name: " + str(lamps['name']) + "\n"
        # print("oneP: "+ oneP)
        counter += 1

    if(counter == 0):
        print("No lamps are reporting broken")
        return
    if(counter != 1):
        print(oneP)
        ilampName = input("Pick a number: ")
    else:
        ilampName = 0
    if ilampName == "":
        print("invalid input, Aborted")
        return

    ilampName = int(ilampName)

    name = str(wlist[ilampName])
    # print("name: "+name)
    if yes_no("Is this lamp fixed?"):
        lampDB.update({'working': True}, lamp['name'] == name)
        today = str(moment.now().format("YYYY-MM-DD"))
        lampDB.update({'installation date': today}, lamp['name'] == name)
        print("DB updated")
    else:
        return
    # number pick
    # update DB


def help():
    return print("""
Help:
    This is lamp, a python script for keeping time on lighting

    Available commands:

    add, a
        Adds lamp to DB

    list, l
        List all lamps and calc the time since installation

    broken, b
        Report a not working lamp

    update, u
        Updates data for lamps

    status, s
        Check and list all lamps that is not working

    remove, r
        remove lamp data

    fix, f
        Mark a lamp as working again
        """)

# START
signal.signal(signal.SIGINT, signal_handler)
print("Ctrl+C = Quit")

i = ""

while i != "q":
    updateDB()
    i = input("\nLamp: ")

    if i == "add" or i == "a":
        add()

    if i == "list" or i == "l":
        list_()

    if i == "broken" or i == "b":
        broken()

    if i == "update" or i == "u":
        update()

    if i == "status" or i == "s":
        status()

    if i == "fix" or i == "f":
        fix()

    if i == "help" or i == "h" or i == "?":
        help()

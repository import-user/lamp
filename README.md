# README
This is lamp.py, a program keeping track over lamps

I hope it is useful when dealing with a lot of lamps

##### TODO 
Update the program 
* set the date in db, last run
* check if data is today
if false:
   git pull and check if there is a update check if user what to update
   quit and start again when done

### How to install and run

#### You need

> git

> python 3+

> (pip for python) Note that pip is included in python

A the moment lamp.py works just with python 3
> git clone https://bitbucket.org/import-user/lamp.git

> cd lamp

> pip3 install -r requirements.txt

> python3 lamp.py

#### Available commands:

    add, a
        Adds lamp to DB

    list, l
        List all lamps and calc the time since installation

    broken, b
        Report a not working lamp

    update, u
        Updates data for lamps

    status, s
        Check and list all lamps that is not working

    remove, r
        remove lamp data

    fix, f
        Mark a lamp as working again

    help, h, ?
        prints this help